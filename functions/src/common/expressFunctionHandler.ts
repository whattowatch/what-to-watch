import * as functions from "firebase-functions";
import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import {FirebaseConfig} from "../modules/firebase/entity/config.entity";
import {NodeEnv} from "./constants.entity";
import {defaultErrorHandler} from "./defaultErrorHandler";

export const expressFunctionHandler = (appProvider: (app: ReturnType<typeof express>) => ReturnType<typeof express>) => functions.https.onRequest(
	appProvider(
		express()
			.use(cors({
				origin: (functions.config() as FirebaseConfig).env.name === NodeEnv.PRODUCTION ?
					(functions.config() as FirebaseConfig).self.domain
					: true,
				credentials: true
			}))
			.use(cookieParser())
	).use(defaultErrorHandler)
);
