import {StatusCode} from "./httpStatusCodes";

export class WtwError extends Error {
	constructor(message: string, public statusCode: StatusCode = StatusCode.INTERNAL_SERVER_ERROR) {
		super(message);
	}
}
