import * as functions from "firebase-functions";
import {NextFunction, Request, Response} from "express";
import {AxiosError} from "axios";
import {StatusCode} from "./httpStatusCodes";
import {WtwError} from "./wrwError";

export const defaultErrorHandler = (error: any, _req: Request, res: Response, _next: NextFunction) => {
	if (error instanceof WtwError) {
		functions.logger.warn("Found wtw error", {error});
		return res.status(error.statusCode).send({message: error.message});
	}

	functions.logger.error("Found an error", {body: (error as AxiosError)?.response?.data}, error);
	return res.status(StatusCode.INTERNAL_SERVER_ERROR).send("KO");
};
