import admin from "firebase-admin";

export const initializeFirebase = () => admin.initializeApp();
