import {NodeEnv} from "../../../common/constants.entity";

export interface FirebaseConfig {
	mal: {
		client_secret: string;
		client_id: string;
	};
	env: {
		name: NodeEnv
	};
	self: {
		domain: string;
	};
}
