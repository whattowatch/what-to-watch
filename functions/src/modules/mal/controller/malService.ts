import {axioMal, axioMalApi} from "./axioMal";
import {AnimeDetailsResponse, TypeAheadResponse} from "./malService.entity";

export const getAnimeDetails = (animeId: string) =>
	axioMalApi.get<AnimeDetailsResponse>(
		`/v2/anime/${animeId}`,
		{
			params: {
				fields: "id,title,alternative_titles,main_picture,rank,popularity,num_list_users,num_scoring_users,recommendations"
			}
		}
	).then(res => res.data);

export const getAnimeTypeAhead = (keyword: string) =>
	axioMal.get<TypeAheadResponse>(
		"/search/prefix.json",
		{
			params: {
				type: "anime",
				keyword,
				v: 1
			}
		}
	).then(res => {
		return res.data;
	});
