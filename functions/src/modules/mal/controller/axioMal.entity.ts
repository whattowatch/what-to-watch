interface BaseApiCallOptions {
	accessToken: string;
}

interface MalStorage extends BaseApiCallOptions {
	refreshToken: string;
}
