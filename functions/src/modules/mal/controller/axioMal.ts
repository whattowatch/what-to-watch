import axios from "axios";
import * as functions from "firebase-functions";
import {FirebaseConfig} from "src/modules/firebase/entity/config.entity";

const malBaseUrl = "https://myanimelist.net";
const malApiBaseUrl = "https://api.myanimelist.net";

export const axioMalApi = axios.create({
	baseURL: malApiBaseUrl,
	headers: {
		"X-MAL-CLIENT-ID": (functions.config() as FirebaseConfig).mal.client_id
	}
});

export const axioMal = axios.create({
	baseURL: malBaseUrl
});
