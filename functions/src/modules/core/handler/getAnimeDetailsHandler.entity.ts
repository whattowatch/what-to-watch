import {ParamsDictionary} from "express-serve-static-core";

export interface GetAnimeDetailsRequestPathParams extends ParamsDictionary {
	animeId: string;
}
