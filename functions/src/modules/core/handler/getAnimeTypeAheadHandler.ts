import * as functions from "firebase-functions";
import {StatusCode} from "../../../common/httpStatusCodes";
import {WtwError} from "../../../common/wrwError";
import {expressFunctionHandler} from "../../../common/expressFunctionHandler";
import {getAnimeTypeAhead} from "../../mal/controller/malService";

export const animeTypeAheadHandler = expressFunctionHandler(app =>
	app.get<{}, {}, {}, {keyword: string}>("/api/v1/anime-type-ahead", async(req, res) => {
		functions.logger.info("anime type ahead call", {query: req.query});

		if(!req.query.keyword)
			throw new WtwError("invalid keyword", StatusCode.BAD_REQUEST);

		const animeTypeAheadResult = await getAnimeTypeAhead(req.query.keyword);

		return res.status(200).send(animeTypeAheadResult);
	})
);
