import * as functions from "firebase-functions";
import {expressFunctionHandler} from "../../../common/expressFunctionHandler";
import {getAnimeDetails} from "../../../modules/mal/controller/malService";
import {GetAnimeDetailsRequestPathParams} from "./getAnimeDetailsHandler.entity";
import {WtwError} from "../../../common/wrwError";
import {StatusCode} from "../../../common/httpStatusCodes";

export const getAnimeDetailsHandler = expressFunctionHandler(app =>
	app.get<GetAnimeDetailsRequestPathParams>(
		"/api/v1/anime/:animeId",
		async(req, res) => {
			functions.logger.info("Get anime details call", {body: req.params});

			if(!req.params.animeId)
				throw new WtwError("invalid anime id", StatusCode.BAD_REQUEST);

			const response = await getAnimeDetails(req.params.animeId);
			functions.logger.info("done");

			return res.status(200).send(response);
		}
	)
);
