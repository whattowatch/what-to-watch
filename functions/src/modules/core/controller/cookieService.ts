import {Response} from "express";
import {MalOAuthTokensResponse} from "../../../modules/mal/controller/malService.entity";
import {Cookie} from "./cookieService.entity";
import * as functions from "firebase-functions";
import {FirebaseConfig} from "../../../modules/firebase/entity/config.entity";
import {NodeEnv} from "../../../common/constants.entity";
import {Request} from "express-serve-static-core";
import {WtwError} from "../../../common/wrwError";
import {StatusCode} from "../../../common/httpStatusCodes";

export const addSessionCookies = (res: Response, cookies: MalOAuthTokensResponse) => {
	res.cookie(Cookie.AccessToken, Buffer.from(JSON.stringify(cookies)).toString("base64"), {
		httpOnly: true,
		maxAge: (cookies.expires_in - 60) * 1000,
		secure: (functions.config() as FirebaseConfig).env.name === NodeEnv.PRODUCTION,
		sameSite: "strict"
	});
};

export const readSessionCookies = (req: Request) => {
	const cookie = req.cookies[Cookie.AccessToken];

	if(!cookie)
		throw new WtwError("Invalid cookie in request", StatusCode.BAD_REQUEST);

	try {
		return JSON.parse(Buffer.from(cookie, "base64").toString("ascii")) as MalOAuthTokensResponse;
	} catch(error) {
		throw new WtwError("Invalid cookie in request", StatusCode.BAD_REQUEST);
	}
};
