import {initializeFirebase} from "./modules/firebase/controller/initializeFirebase";
export * from "./modules/core/handler/getAnimeDetailsHandler";
export * from "./modules/core/handler/getAnimeTypeAheadHandler";

initializeFirebase();
