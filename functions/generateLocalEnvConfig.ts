console.log(JSON.stringify(
	{
		mal: {
			client_secret: process.env.CLIENT_SECRET,
			client_id: process.env.CLIENT_ID
		},
		env: {
			name: process.env.CI_ENVIRONMENT_NAME
		},
		domain: {
			name: process.env.SELF_DOMAIN
		}
	},
	undefined, 2
));
