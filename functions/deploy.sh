#!/bin/bash

if [ -z $CI ]; then
	echo "Can only run in CI context."
	exit 1
fi

if [ "$CI_ENVIRONMENT_NAME" != "production" ]; then
	echo "Enviroment name is wrong, should be dev or prod, is $CI_ENVIRONMENT_NAME"
	exit 1
fi

firebase functions:config:set mal.client_secret="$CLIENT_SECRET" &
firebase functions:config:set mal.client_id="$CLIENT_ID" &
firebase functions:config:set env.name="$CI_ENVIRONMENT_NAME" &
firebase functions:config:set self.domain="$SELF_DOMAIN" &
wait

firebase deploy --only functions,hosting -m "$GITLAB_USER_LOGIN - $CI_COMMIT_SHA"
