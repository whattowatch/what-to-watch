export interface AnimeDetailsResponse {
	id: number;
	title: string;
	main_picture: {
		medium: string;
		large: string;
	};
	alternative_titles: {
		synonyms: string[];
		en: string;
		ja: string;
	};
	start_date: string;
	end_date: string;
	synopsis: string;
	mean: number;
	rank: number;
	popularity: number;
	num_list_users: number;
	num_scoring_users: number;
	nsfw: string;
	created_at: string;
	updated_at: string;
	media_type: string;
	status: string;
	genres: {
		id: number;
		name: string;
	}[];
	my_list_status: {
		status: string;
		score: number;
		num_episodes_watched: number;
		is_rewatching: boolean;
		updated_at: string;
	};
	num_episodes: number;
	recommendations: {
		node: {
			id: number;
			title: string;
			main_picture: {
				medium: string;
				large: string;
			};
		};
		num_recommendations: number;
	}[];
	studios: {
		id: number;
		name: string;
	}[];
	statistics: {
		status: {
			watching: `${number}`;
			completed: `${number}`;
			on_hold: `${number}`;
			dropped: `${number}`;
			plan_to_watch: `${number}`;
		};
		num_list_users: number;
	};
}

export enum TypeAheadTypes {
	ANIME = "anime",
	MANGA = "mange",
}

export interface TypeAheadResponse {
	categories: {
		type: TypeAheadTypes,
		items: {
			id: number,
			type: string;
			name: string;
			url: string;
			image_url: string;
			thumbnail_url: string;
			payload: {
				media_type: string;
				start_year: number;
				aired: string;
				score: string;
				status: string;
			},
			es_score: number;
		}[]
	}[]
}
