import {AnimeDetailsResponse, TypeAheadResponse} from "./malApiService.entity";
import {axioBFF} from "./axioBFF";

export const getAnimeDetails = (animeId: string = "30230") =>
	axioBFF.get<AnimeDetailsResponse>(`/api/v1/anime/${animeId}`).then(res => res.data);

export const getAnimeTypeAhead = (keyword: string) =>
	axioBFF.get<TypeAheadResponse>(
		"/api/v1/anime-type-ahead",
		{params: {keyword}}
	).then(res =>
		res.data
	);
