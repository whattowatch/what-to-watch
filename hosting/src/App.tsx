import {useCallback, useMemo, useState} from "react";
import {BrowserRouter} from "react-router-dom";
import "./App.css";
import {debounce} from "./common/utils/debounce";
import {MalNode, MalGraph} from "./components/graph/Graph";
import {useGraphData} from "./components/graph/Graph.hooks";
import {getAnimeDetails, getAnimeTypeAhead} from "./services/malApiService";
import Autocomplete from "@mui/material/Autocomplete";
import {AutocompleteChangeReason, AutocompleteInputChangeReason} from "@mui/core";
import TextField from "@mui/material/TextField";
import {throttle} from "./common/utils/throttle";
import {styled} from "@mui/system";
import Box from "@mui/material/Box";
import UseAnimations from "react-useanimations";
import settingsAnimation from "react-useanimations/lib/settings"
import SpeedDial from "@mui/material/SpeedDial";
import SpeedDialAction from "@mui/material/SpeedDialAction";
import LightMode from "@mui/icons-material/LightMode";
import Translate from "@mui/icons-material/Translate";
import {themeEntity, toggleTheme} from "./components/configs/theme.store";
import {languageEntity, toggleLanguage} from "./components/configs/language.store";
import {createTheme, ThemeProvider} from "@mui/material/styles";
import {useEntity} from "simpler-state";
import {DarkMode} from "@mui/icons-material";

const CustomAutocomplete = styled(Autocomplete)`
  pointer-events: auto;
  height: min-content;
  margin: 14px;
  border-width: 3px;
  border-color: black;
` as typeof Autocomplete;

function App() {
  const [graphData, {appendNode, linkNodes}] = useGraphData<MalNode>();
  const [animeSearchList, setAnimeSearchList] = useState<{label: string; id: string}[]>([]);
  const themeStore = useEntity(themeEntity);
  const languageStore = useEntity(languageEntity);
  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode: themeStore.main
        }
      }),
    [themeStore.main]
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const throttledGetAnimeTypeAhead = useCallback(throttle(getAnimeTypeAhead, 1000), []);

  const appendNewAnimeNodes = useCallback((animeId: string) =>
    getAnimeDetails(animeId).then(res => {
      const image = new Image();
      image.src = res.main_picture.medium;

      appendNode({id: res.id, image, name: res.title, enName: res.alternative_titles.en || res.title});

      res.recommendations.forEach(recommendation => {
        const recommendationImage = new Image();
        recommendationImage.src = recommendation.node.main_picture.medium;

        appendNode({id: recommendation.node.id, image: recommendationImage, name: recommendation.node.title, enName: recommendation.node.title});
        linkNodes(res.id, recommendation.node.id);
      });
    }),
    [appendNode, linkNodes]
  );

  const updateSearchListWithKeyword = useCallback(async(
    event: React.SyntheticEvent,
    value: string,
    reason: AutocompleteInputChangeReason,
  ) => {
    if(!value)
      return;

    if(reason === "reset" || reason === "clear")
      return setAnimeSearchList([]);

    if(reason !== "input")
      return;

    const result = await throttledGetAnimeTypeAhead(value);

    setAnimeSearchList(
      result.categories.flatMap(category => category.items.map(item => ({
        id: String(item.id),
        label: item.name,
      })))
    );
  }, [setAnimeSearchList, throttledGetAnimeTypeAhead]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onAutocompleteSelect = useCallback(throttle((
    event: React.SyntheticEvent,
    value: {label: string; id: string} | null,
    reason: AutocompleteChangeReason,
  ) => {
    if(reason !== "selectOption" || !value)
      return;
    
    appendNewAnimeNodes(value.id);
  }), [appendNewAnimeNodes]);

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Box
          sx={{
            minHeight: "100vh",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            fontSize: "calc(10px + 2vmin)",
            bgcolor: 'background.default',
            color: 'text.primary'
          }}
        >
          <div className="fullscreen-space">
            <MalGraph
              graphData={graphData}
              onNodeClick={debounce((node) => {
                const customNode = node as Required<MalNode>;

                appendNewAnimeNodes(customNode.id as string);
              }, 1000)}
            ></MalGraph>
          </div>
          <div className="fullscreen-space right-align">
            <a target="_blank" rel="noreferrer" className="buy-coffee" href="https://www.buymeacoffee.com/ranguna">
              <img alt="Support me by buying me a tea" src="https://img.buymeacoffee.com/button-api/?text=Buy me a tea&emoji=🍵&slug=ranguna&button_colour=FFDD00&font_colour=000000&font_family=Poppins&outline_colour=000000&coffee_colour=ffffff"></img>
            </a>
          </div>
          <div className="fullscreen-space search">
            <CustomAutocomplete
              sx={{width: 300, bgcolor: 'background.default'}}
              autoComplete
              renderInput={(params) => <TextField {...params} label="Anime" placeholder="Search animes..." />}
              options={animeSearchList}
              onInputChange={updateSearchListWithKeyword}
              onChange={onAutocompleteSelect}
            ></CustomAutocomplete>
            <SpeedDial
              ariaLabel="Settings"
              icon={<UseAnimations color="white" animation={settingsAnimation} size={38} strokeColor="white" style={{padding: 100}} />}
              direction="down"
            >
              <SpeedDialAction
                key="theme"
                icon={themeStore.main === "dark" ? <DarkMode /> : <LightMode />}
                tooltipTitle={`Theme: ${themeStore.main}`}
                onClick={toggleTheme}
              />
              <SpeedDialAction
                key="language"
                icon={<Translate />}
                tooltipTitle={`Language: ${languageStore.language}`}
                onClick={toggleLanguage}
              />
            </SpeedDial>
          </div>
        </Box>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
