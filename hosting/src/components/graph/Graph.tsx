import {FC} from 'react';
import ForceGraph, {ForceGraphProps, GraphData, NodeObject} from 'react-force-graph-2d';
import {useEntity} from 'simpler-state';
import {languageEntity} from '../configs/language.store';
import {themeEntity} from '../configs/theme.store';

export type GraphDataWithNode<Node extends object> = GraphData & {nodes: (NodeObject & Node)[]};

export interface GraphProps<Node extends object = {}> {
	graphData: GraphDataWithNode<Node>;
	size?: number;
	onNodeClick?: ForceGraphProps<Node>["onNodeClick"];
}

export interface MalNode extends NodeObject{
	image: HTMLImageElement;
	name: string;
	enName: string;
}

export const MalGraph: FC<GraphProps<MalNode>> = ({graphData, size = 12, onNodeClick}) => {
	const theme = useEntity(themeEntity);
	const language = useEntity(languageEntity);

	return <ForceGraph
		linkColor={() => theme.main === "dark" ? "#ffffffd8" : "#00000026"}
		graphData={graphData}
		nodeCanvasObjectMode={() => "replace"}
		nodeRelSize={size}
		nodeCanvasObject={(obj, ctx) => {
			const customNode = obj as Required<MalNode>; 
			let imgWidth = size * customNode.image.width/customNode.image.height
			let imgHeight = size * customNode.image.height/customNode.image.width;
			if(customNode.image.height > customNode.image.width) {
				imgWidth = size
			} else {
				imgHeight = size
			}
			ctx.save()
			ctx.beginPath()
			ctx.arc(customNode.x, customNode.y, size/2, 0, 2 * Math.PI);
			ctx.clip();
			ctx.drawImage(customNode.image, customNode.x - imgWidth/2, customNode.y - imgHeight / 2, imgWidth, imgHeight)
			ctx.restore();
		}}
		nodePointerAreaPaint={(node, color, ctx) => {
			ctx.fillStyle = color;
			ctx.fillRect(node.x! - size / 2, node.y! - size / 2, size, size);
		}}
		onNodeClick={onNodeClick as any}
		nodeLabel={(node: any) => (language.language === "JP" ? node.name : node.enName) as keyof MalNode}
	></ForceGraph>;
}