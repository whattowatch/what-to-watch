import {useState} from "react"
import {NodeObject} from "react-force-graph-2d";
import {GraphDataWithNode} from "./Graph";

interface InternalGraphState<CustomNode extends NodeObject> {
  graphData: GraphDataWithNode<CustomNode>;
  nodeMap: Record<string, Record<string, true>>;
}

export const useGraphData = <CustomNode extends NodeObject>(initialValue?: Partial<GraphDataWithNode<CustomNode>>) => {
	const [graphState, setGraphData] = useState<InternalGraphState<CustomNode>>({
    graphData: {
      nodes: [
        ...(initialValue?.nodes ?? [])
      ],
      links: [
        ...(initialValue?.links ?? [])
      ]
    },
    nodeMap: {}
  });

	return [
		graphState.graphData,
		{
			appendNode: (additionalNode: CustomNode) => setGraphData(currentData => {
        if(currentData.nodeMap[String(additionalNode.id)])
          return currentData;

        return {
          graphData: {
            ...currentData.graphData,
            nodes: [
              ...currentData.graphData.nodes,
              additionalNode
            ]
          },
          nodeMap: {
            ...currentData.nodeMap,
            [String(additionalNode.id)]: {}
          }
        }
      }),
			linkNodes: (sourceNodeId: CustomNode["id"], targetNodeId: CustomNode["id"]) => setGraphData(currentData => {
        if(!currentData.nodeMap[String(sourceNodeId)] || currentData.nodeMap[String(sourceNodeId)][String(targetNodeId)])
          return currentData;

        return {
          graphData: {
            ...currentData.graphData,
            links: [
              ...currentData.graphData.links,
              {
                source: sourceNodeId,
                target: targetNodeId
              }
            ]
          },
          nodeMap: {
            ...currentData.nodeMap,
            [String(sourceNodeId)]: {
              ...currentData.nodeMap[String(sourceNodeId)],
              [String(targetNodeId)]: true
            }
          }
        };
      })
		}
	] as const;
}