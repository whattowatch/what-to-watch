import {entity} from "simpler-state";

interface Language {
	language: "JP" | "EN";
}

export const languageEntity = entity<Language>({language: "JP"});


export const toggleLanguage = () => languageEntity.set((oldValue) => ({
	...oldValue,
	language: oldValue.language === "JP" ? "EN" : "JP"
}))
