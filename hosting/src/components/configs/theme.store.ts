import {entity} from "simpler-state";

interface Theme {
	main: "dark" | "light";
}

export const themeEntity = entity<Theme>({main: "dark"});

export const toggleTheme = () => themeEntity.set((oldValue) => ({
	...oldValue,
	main: oldValue.main === "dark" ? "light" : "dark"
}))
