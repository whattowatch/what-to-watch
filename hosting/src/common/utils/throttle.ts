export const throttle = <T extends any[], R>(func: (...args: T) => R, throttleWindow = 500) => {
	let timer: number | null = null;

	return (...args: T) => new Promise<R>(resolve => {
		if (timer) clearTimeout(timer);

		timer = window.setTimeout(() => {
			timer = null;
			resolve(func(...args))
		}, throttleWindow);
	});
};