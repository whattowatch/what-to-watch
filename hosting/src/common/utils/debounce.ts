export const debounce = <T extends any[], R>(func: (...args: T) => R, debounceWindow = 500) => {
	let timer: number | null = null;

	return (...args: T) => new Promise<R>(resolve => {
		if (timer) clearTimeout(timer);
		else resolve(func(...args));

		timer = window.setTimeout(() => {
			timer = null;
		}, debounceWindow);
	});
};