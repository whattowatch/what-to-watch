import {useLocation} from "react-router-dom";
import qs from 'qs';

export const useQueryString = <T>() => [qs.parse(useLocation().search.replace(/\?/g, '')) as Partial<T>];
